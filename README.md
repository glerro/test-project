# TEST PROJECT DO NOT DOWNLOAD

![Screenshoot_01 ><](https://gitlab.gnome.org/glerro/test-project/-/raw/main/assets/Screenshot_from_2023-09-28_23-40-49.webp){: style="center"}

![Screenshoot_02](https://gitlab.gnome.org/glerro/test-project/-/raw/main/assets/Screenshot_GSE.webp){: style="float:center"}

Toggle the Looking Glass visibility by right clicking on a panel icon.

And from version 4 left clicking on the icon show a menu with new features like 
Restart Gnome Shell (not available on Wayland), Reload Theme, Open Extension
Folder and Open Theme Folder (the last two require that xdg-open is installed).

Version 4 also drop the compatibility with Gnome Shell 3.30.

## Features
* Toggle the Looking Glass visibility by right clicking on a panel icon.
* Show a menu by left clicking on a panel icon with usefull shortcuts like:
   * Restart Gnome Shell (not available on Wayland).
   * Reload Theme.
 * Open Extension Folder (require xdg-open).
 * Open Theme Folder (require xdg-open).
* Support Gnome Shell Versions 3.32, 3.34, 3.36, 3.38, 40, 41, 42, 43.
* Translatable.

## Translation Status
[![Stato traduzione ><](https://translate.codeberg.org/widget/test-project/multi-auto.svg)](https://translate.codeberg.org/engage/test-project/)

## Installation
Normal users are recommended to get the extension from

[![Get it on GNOME Extensions](https://gitlab.gnome.org/glerro/gnome-shell-extension-lgbutton/-/raw/main/assets/get-it-on-gnome-extensions.svg)](https://extensions.gnome.org/extension/2296/looking-glass-button/)

## Manual Installation
You can check out the latest version with git, build and install it with [Meson](https://mesonbuild.com/).

You must install git, meson, ninja and xgettext.

For a regular use and local installation these are the steps:

```bash
git clone https://gitlab.gnome.org/glerro/gnome-shell-extension-lgbutton.git
cd gnome-shell-extension-lgbutton
meson setup --prefix=$HOME/.local _build
meson install -C _build
```

Otherwise you can run the `./local_install.sh` script, it performs the build steps specified in the
previous section.

You may need to restart GNOME Shell (<kbd>Alt</kbd>+<kbd>F2</kbd>, <kbd>r</kbd>, <kbd>⏎</kbd>)
after that, or logout and login again.

## Export extension ZIP file

To create a ZIP file with the extension, just run:

```bash
git clone https://gitlab.gnome.org/glerro/gnome-shell-extension-lgbutton.git
cd gnome-shell-extension-lgbutton
./export-zip.sh
```

This will create the file `lgbutton@glerro.gnome.gitlab.io.shell-extension.zip` with the extension, you can install and enable it with this steps:

```bash
gnome-extensions install lgbutton@glerro.gnome.gitlab.io.shell-extension.zip
gnome-extensions enable lgbutton@glerro.gnome.gitlab.io
```

You may need to restart GNOME Shell (<kbd>Alt</kbd>+<kbd>F2</kbd>, <kbd>r</kbd>, <kbd>⏎</kbd>)
after that, or logout and login again.

## -----------------------
## Translating
To contribute translations you will need a Gnome GitLab account, git, meson, ninja, xgettext and a translation program like Gtranslator.

Start by creating a fork on Gnome GitLab.

Clone your fork with git and setup a project with meson:

```bash
git clone https://gitlab.gnome.org/<your username>/gnome-shell-extension-lgbutton.git
cd gnome-shell-extension-lgbutton
meson setup --prefix=$HOME/.local _build
meson compile -C _build
```

Ensure the translation template is uptodate:

```bash
meson compile -C _build gnome-shell-extension-lgbutton-pot
meson compile -C _build gnome-shell-extension-lgbutton-update-po
```

Open /po/gnome-shell-extension-lgbutton.pot whith Gtranslator or a similar program and translate, and translate all messages. Save your translation as /po/<language code>.po with the others, then add the language code for your translation is in /po/LINGUAS file. For example, if your translation is named fr.po, data/po/LINGUAS should have fr on a separate line in it.


To test your translation, install the extension and restart GNOME Shell:

meson install -C _build
If you're unsure whether you use X11/Xorg or Wayland, enter:

echo $XDG_SESSION_TYPE
Restart:

X11/Xorg: press Alt + F2 and enter restart.
Wayland: log out and log in.



When you're happy with your translation, commit the changes and push them to your fork:

git commit -a -m "New translations (French)"
git push

Finally, open a New Merge Request from your fork at https://gitlab.com/<your_username>/gnome-shell-extension-task-widget:


-----------

Packaging

## License
Looking Glass Button - Copyright (c) 2019-2022 Gianni Lerro {glerro} ~ <glerro@pm.me>

Looking Glass Button is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Looking Glass Button is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with Looking Glass Button. If not, see <http://www.gnu.org/licenses/>.

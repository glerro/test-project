#!bin/sh

xgettext --from-code=UTF-8 -D lgbutton@glerro.gnome.gitlab.io -o lgbutton.pot -p po --language=JavaScript --keyword='_' --add-comments=Translators  --copyright-holder="Copyright (c) 2019-2022 Gianni Lerro {glerro} ~ <glerro@pm.me>" --package-name=gnome-shell-extension-lgbutton --package-version=6 --msgid-bugs-address="https://gitlab.gnome.org/glerro/gnome-shell-extension-lgbutton/issues" *.js


#!/bin/bash

rm -rf _build
meson setup --prefix=$HOME/.local _build
meson install -C _build
rm -rf _build

/* -*- Mode: js; indent-tabs-mode: nil; js-basic-offset: 4; tab-width: 4; -*- */
/*
 * This file is part of Looking Glass Button.
 * https://gitlab.gnome.org/glerro/gnome-shell-extension-lgbutton
 *
 * extension.js
 *
 * Copyright (c) 2019-2022 Gianni Lerro {glerro} ~ <glerro@pm.me>
 *
 * Looking Glass Button is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Looking Glass Button is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Looking Glass Button. If not, see <http://www.gnu.org/licenses/>.
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: 2019-2022 Gianni Lerro <glerro@pm.me>
 */

/* exported init */

'use strict';

const Gettext = imports.gettext;

const {Clutter, Gio, GLib, GObject, Meta, St} = imports.gi;

const Main = imports.ui.main;
const PanelMenu = imports.ui.panelMenu;
const PopupMenu = imports.ui.popupMenu;
casso
const Util = imports.misc.util;
const ExtensionUtils = imports.misc.extensionUtils;
const Me = ExtensionUtils.getCurrentExtension();

// For compatibility checks, as described above
const Config = imports.misc.config;
const SHELL_MAJOR = parseInt(Config.PACKAGE_VERSION.split('.')[0]);
const SHELL_MINOR = parseInt(Config.PACKAGE_VERSION.split('.')[1]);

const ExtensionName = Me.metadata.name;
const ExtensionVersion = Me.metadata.version;

const Domain = Gettext.domain(Me.metadata['gettext-domain']);
const _ = Domain.gettext;

const ExtensionsPath = GLib.build_pathv('/', [global.userdatadir, 'extensions/']);
const ThemesPath = GLib.build_pathv('/', [GLib.get_user_data_dir(), 'themes/']);
const XdgOpenPath = GLib.find_program_in_path('xdg-open');
const isWayland = Meta.is_wayland_compositor();

// Extend the Button class from Panel Menu.
let LGIndicator = GObject.registerClass({
    GTypeName: 'LGIndicator',
}, class LGIndicator extends PanelMenu.Button {
    _init() {
        super._init(0.0, `${ExtensionName} Indicator`, false);

        // Pick an icon
        this._icon = new St.Icon({
            gicon: Gio.icon_new_for_string(`${Me.path}/icons/utilities-looking-glass-symbolic.svg`),
            style_class: 'system-status-icon',
        });

        // Right Click Menu
        this.rightClickMenu = new RightClickMenu(this, 0.5, St.Side.TOP);

        // Main Menu Manager
        this.menuManager = new PopupMenu.PopupMenuManager(this);
        this.menuManager._changeMenu = _menu => { };
        this.menuManager.addMenu(this.rightClickMenu);

        this.add_actor(this._icon);
    }

    vfunc_event(event) {
        if (event.type() === Clutter.EventType.BUTTON_PRESS) {
            if (event.get_button() === 1) {
                if (this.rightClickMenu.isOpen)
                    this.rightClickMenu.toggle();

                if (Main.lookingGlass === null)
                    Main.createLookingGlass();

                Main.lookingGlass.toggle();
            } else if (event.get_button() === 3) {
                // Compatibility with gnome-shell = 3.32
                if (SHELL_MINOR > 32) {
                    if (Main.lookingGlass !== null && Main.lookingGlass.isOpen)
                        Main.lookingGlass.toggle();
                } else if (Main.lookingGlass !== null && Main.lookingGlass._open) {
                    Main.lookingGlass.toggle();
                }
                this.rightClickMenu.toggle();
            }
            return Clutter.EVENT_STOP;
        }
        return Clutter.EVENT_PROPAGATE;
    }
});

var RightClickMenu = class LGIndicatorRightClickMenu extends PopupMenu.PopupMenu {
    constructor(sourceActor, arrowAlignment, arrowSide) {
        super(sourceActor, arrowAlignment, arrowSide);

        Main.uiGroup.add_actor(this.actor);
        this.actor.hide();

        let paramsW, paramsE, paramsT = { };

        // Restart Gnome Shell is not available on Wayland
        if (isWayland)
            paramsW = {reactive: false, can_focus: false};

        // Restart Gnome Shell
        log(_('123 prova'));
        this.restartShell = new PopupMenu.PopupMenuItem(_('Restart Shell'), paramsW);
        this.addMenuItem(this.restartShell);
        this.restartShell.connect('activate', () => {
            if (SHELL_MAJOR < 43)
                Meta.restart(_('Restarting…'));
            else
                Meta.restart(_('Restarting…'), global.context);
        });

        // Reload Gnome Shell Theme
        this.reloadShellTheme = new PopupMenu.PopupMenuItem(_('Reload Theme'));
        this.addMenuItem(this.reloadShellTheme);
        this.reloadShellTheme.connect('activate', () => {
            Main.reloadThemeResource();
            Main.loadTheme();
        });

        // Separator
        this.addMenuItem(new PopupMenu.PopupSeparatorMenuItem());

        // Check if xdg-open is installed
        if (!XdgOpenPath)
            paramsE = paramsT = {reactive: false, can_focus: false};

        // Check if Extensions Folder exist
        if (!GLib.file_test(ExtensionsPath, GLib.FileTest.EXISTS))
            paramsE = {reactive: false, can_focus: false};

        // Open Gnome Shell Extensions folder
        this.extensionsFolder = new PopupMenu.PopupMenuItem(_('Open Extensions Folder'), paramsE);
        this.addMenuItem(this.extensionsFolder);
        this.extensionsFolder.connect('activate', () => {
            Util.trySpawnCommandLine(`xdg-open ${ExtensionsPath}`);
        });

        // Check if Themes Folder exist
        if (!GLib.file_test(ThemesPath, GLib.FileTest.EXISTS))
            paramsT = {reactive: false, can_focus: false};

        // Open Gnome Shell Themes folder
        this.themesFolder = new PopupMenu.PopupMenuItem(_('Open Themes Folder'), paramsT);
        this.addMenuItem(this.themesFolder);
        this.themesFolder.connect('activate', () => {
            Util.trySpawnCommandLine(`xdg-open ${ThemesPath}`);
        });
    }
};

class Extension {
    constructor() {
        this._indicator = null;
    }

    enable() {
        log(`Enabling ${ExtensionName} - Version ${ExtensionVersion}`);

        this._indicator = new LGIndicator();

        Main.panel.addToStatusArea(`${ExtensionName} Indicator`, this._indicator);
    }

    disable() {
        log(`Disabling ${ExtensionName} - Version ${ExtensionVersion}`);

        if (this._indicator !== null) {
            this._indicator.destroy();
            this._indicator = null;
        }
    }
}

/**
 * This function is called once when extension is loaded, not enabled.
 *
 * @param {ExtensionMeta} meta - An extension meta object.
 * @returns {Extension} - the extension object with enable() and disable() methods.
 */
function init(meta) {
    log(`Inizializing ${meta.metadata.name} - Version ${meta.metadata.version}`);

    // Inizializing translations
    ExtensionUtils.initTranslations();

    return new Extension();
}
